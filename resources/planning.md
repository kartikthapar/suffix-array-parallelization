- understand and design an overall view of the algorithm - what parts need to parallelized
- compare Map-Reduce vs native parallelization in C/C++
- 

# Presentation

# Project Document

- Analysis of pDC3.
- Implementing pDC3 in Map-Reduce
    - Parallelizable parts
    - native vs Map-Reduce
    - Radix Sort on one machine vs Radix Sort on multiple machines vs Map-Reduce for sort
- Performance
- Scalability
- Efficient Map-Reduce
- Conclusion

* TasK: unique names? if possible?
* Another task: Recursive Map-Reduce