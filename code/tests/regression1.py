#!/usr/bin/python
#
# booktest.py
#
# Created by Kartik Thapar on 12/04/2013 at 03:11:19
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import random
import sys
sys.path.append("..")
from bin import linearmrdc3
from bin import suffixarray
import cProfile

def generateGenomeOfSize(size):
    return ''.join([random.choice('actg') for i in range(0, size)])

def calculatesuffixarrays():
    x = generateGenomeOfSize(10000)
    d = linearmrdc3.calc(x)
    n = suffixarray.calc(x)
    print d == n

cProfile.run('calculatesuffixarrays()')
